title: About
summary: Just another blog
date: 2017-09-10 21:00
tags: blog
---

I post about:

- [Emacs](https://www.gnu.org/software/emacs/) - The extensible,
  customizable, self-documenting text editor.
- [Guix](https://www.gnu.org/software/guix/) - Functional package
  manager for installed software packages and versions.
- [Guile](https://www.gnu.org/software/guile/) - Scheme implementation
  intended especially for extensions.
- [Haunt](https://haunt.dthompson.us/) - Static site generator written
  in Guile Scheme.
- [Replicant](https://replicant.us/) - Free software operating system
  based on the Android.
- [Stumpwm](https://github.com/stumpwm/stumpwm) - Window manager
  inspired by [Ratpoison](https://www.nongnu.org/ratpoison/) and
  written in Common Lisp.

For questions mail me [go.wigust@gmail.com](mailto:go.wigust@gmail.com).
