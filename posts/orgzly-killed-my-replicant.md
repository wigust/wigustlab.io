title: Recover replicant after Orgzly upgrade
summary: Version 1.6 of Orgzly killed my system
date: 2017-11-18 7:40
tags: replicant org-mode
---

I got a bootloop after upgrading Orgzly to version 1.6 from
[F-Droid](https://f-droid.org/) on my [Replicant](https://replicant.us/) device.
Thanks to
[Amarpreet's message](https://redmine.replicant.us/boards/9/topics/14618?r=14622#message-14622)
pointing to
[Asara's message](https://forums.bitfire.at/topic/1603/replicant-lineage-1-9-1-causing-instant-crash-and-bootloops)
I got an idea how to fix it.

You need an
[adb](https://developer.android.com/studio/command-line/adb.html)
installed on your PC's operating system.

Boot your Replicant device to recovery.

Find an Android device:

```
natsu@magnolia ~$ adb devices
List of devices attached
4df7772e5a57cfbd    recovery
```

Get into a sh shell:

```
natsu@magnolia ~$ adb shell
```

Find out /data mount point:

We need ext4 one.

```
root@t03g:/ # cat /etc/fstab
/dev/block/mmcblk0p3 /efs ext4 journal_async_commit,errors=panic 0 0
/dev/block/mmcblk0p13 /system ext4 defaults 0 0
/dev/block/mmcblk0p12 /cache f2fs discard,inline_xattr,inline_data 0 0
/dev/block/mmcblk0p12 /cache ext4 journal_async_commit,errors=panic 0 0
/dev/block/mmcblk0p14 /preload ext4 journal_async_commit 0 0
/dev/block/mmcblk0p16 /data f2fs discard,inline_xattr,inline_data 0 0
/dev/block/mmcblk0p16 /data ext4 noauto_da_alloc,journal_async_commit,errors=panic 0 0
```

Mount /data file system:

```
root@t03g:/ # mount /dev/block/mmcblk0p16 /data

root@t03g:/ # ls /data
ISP_CV app-asec    bootchart    data  log        misc           ss         user 
adb    app-lib     bugreports   drm   lost+found property       ssh        
anr    app-private cfw          gps   media      resource-cache system     
app    backup      dalvik-cache local mediadrm   security       tombstones
```

Find Orgzly:

```
root@t03g:/ # ls /data/app
com.github.axet.callrecorder-1 com.orgzly-2 org.videolan.vlc-1 org.xbmc.kore-1
```

Backup Orgzly to PC:

Open another terminal window on PC and make a backup of your Orgzly
just in case.

```
natsu@magnolia ~$ adb pull /data/app/com.orgzly-2 .
```

Remove Orgzly on Replicant device:

```
root@t03g:/ # rm -rf /data/app/com.orgzly-2
```

Check if it was removed:

```
root@t03g:/ # ls /data/app/
com.github.axet.callrecorder-1 org.videolan.vlc-1 org.xbmc.kore-1 
```

Make sure the kernel wrote to file system:

```
root@t03g:/ # sync 
```

Unmount /data file system:

```
root@t03g:/ # unmount /data
```

Reboot:

```
root@t03g:/ # reboot
```

