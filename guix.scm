;; Copyright © 2018 Oleg Pykhalov <go.wigust@gmail.com>
;; Released under the GNU GPLv3 or any later version.

(use-modules ((guix licenses) :prefix license:)
             (gnu packages base)
             (gnu packages compression)
             (gnu packages guile)
             (gnu packages version-control)
             (gnu packages)
             (guix build-system trivial)
             (guix download)
             (guix packages)
             (ice-9 ftw)
             (srfi srfi-26))

(define-public twbs
  (package
    (name "twbs")
    (version "3.3.5")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/twbs/bootstrap/archive/v"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rzyk6aip376mb7mfgpp8920f9k9xkrijzp2n23qy03nkpl26dyv"))))
    (build-system trivial-build-system)
    (outputs '("out" "dist"))
    (native-inputs
     `(("tar" ,tar)
       ("gzip" ,gzip)))
    (arguments
     `(#:modules
       ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         ;; unpack
         (setenv "PATH" (string-append
                         (assoc-ref %build-inputs "tar") "/bin" ":"
                         (assoc-ref %build-inputs "gzip") "/bin"))
         (invoke "tar" "xvf" (assoc-ref %build-inputs "source"))
         ;; install
         (chdir (string-append "bootstrap" "-" ,version))
         (copy-recursively "." (string-append (assoc-ref %outputs "out")
                                              "/share/bootstrap"))
         ;; dist
         (let ((install-files (lambda (files directory)
                                (for-each (lambda (file)
                                            (install-file file
                                                          directory))
                                          files)))
               (static (string-append (assoc-ref %outputs "dist")
                                      "/share/static")))
           (with-directory-excursion "dist"
             (with-directory-excursion "css"
               (install-files '("bootstrap.min.css" "bootstrap-theme.min.css")
                              (string-append static "/css")))
             (with-directory-excursion "js"
               (install-file "bootstrap.min.js"
                             (string-append static "/js"))))))))
    (home-page "https://github.com/twbs/bootstrap/")
    (properties '((upstream-name . "bootstrap")))
    (synopsis "HTML, CSS, JavaScript framework")
    (description
     "This package provides HTML, CSS, and JavaScript framework for
developing responsive, mobile first projects on the web.")
    (license license:expat)))

(define-public jquery-min
  (package
    (name "jquery-min")
    (version "1.11.3")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://code.jquery.com/jquery-"
                                  version ".min.js"))
              (sha256
               (base32
                "1f4glgxxn3jnvry3dpzmazj3207baacnap5w20gr2xlk789idfgc"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules
       ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils)
                      (ice-9 ftw)
                      (srfi srfi-26))
         (let ((js (string-append (assoc-ref %outputs "out")
                                  "/share/static/js")))
           (mkdir-p js)
           (with-directory-excursion js
             (copy-file (assoc-ref %build-inputs "source")
                        (string-append
                         (string-drop-right ,name (string-length "-min"))
                         ".min.js")))))))
    (home-page "https://jquery.com/")
    (synopsis "Simplify the client-side scripting of HTML")
    (description "This package provides a JavaScript library to
simplify the client-side scripting of HTML")
    (license license:expat)))

(package
  (name "haunt-static")
  (version "0")
  (build-system trivial-build-system)
  (source #f)
  (propagated-inputs
   `(("haunt" ,haunt)
     ("python-ghp-import" ,python-ghp-import)
     ("twbs" ,twbs "dist")
     ("jquery-min" ,jquery-min)))
  (arguments
   `(#:modules
     ((guix build utils))
     #:builder
     ;; Stupid block does nothing.
     (begin
       (use-modules (guix build utils))
       (let ((static (string-append (assoc-ref %outputs "out")
                                    "/share/static")))
         (mkdir-p static)
         #t))))
  (synopsis "Static site data for Haunt")
  (description "This package provides statis data like CSS and JS for blog.")
  (license #f)
  (home-page #f))
