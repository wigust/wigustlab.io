;; Copyright © 2018 Oleg Pykhalov <go.wigust@gmail.com>
;; Released under the GNU GPLv3 or any later version.

(use-modules (haunt site)
             (haunt reader)
             (haunt reader commonmark)
             (haunt post)
             (haunt builder assets)
             (haunt builder atom)
             (haunt builder blog))

(define (bootstrap-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (title ,(string-append title " — " (site-title site)))
     (link (@ (rel "stylesheet")
              (type "text/css")
              (href "/static/css/bootstrap.min.css")))
     (link (@ (rel "stylesheet")
              (type "text/css")
              (href "/static/css/bootstrap-theme.min.css")))
     (link (@ (rel "stylesheet")
              (href "/static/css/wigust.css")))
     (link (@ (rel "shortcut icon")
              (type "image/png")
              (href "/static/img/favicon.png"))))
    (body
     (script (@ (src "/static/js/jquery.min.js")))
     (script (@ (src "/static/js/bootstrap.min.js")))
     (div (@ (class "container"))
          ,navbar
          ,body
          (div (@ (class "footer"))
               (div (@ (class "text-center"))
                    (p (@ (class "text-muted")) "Build with ❤ by "
                       (a (@ (href "https://haunt.dthompson.us/"))
                          "Haunt")
                       " & "
                       (a (@ (href "https://www.gnu.org/software/guile/"))
                          "Guile"))))))))

(define navbar
  `(nav (@ (class "navbar"))
        (div (@ (class "navbar-header"))
             (a (@ (href "/") (class "navbar-brand")) "Index"))
        (div (@ (class "collapse navbar-collapse")
                (id "wi-navbar"))
             (ul (@ (class "nav navbar-nav navbar-right"))
                 (li (@ (class "presentation"))
                     (a (@ (href "/feed.xml")) "RSS"))
                 (li (@ (class "presentation"))
                     (a (@ (href "/about.html")) "About"))))))

(define (bootstrap-post-template post)
  `((div (@ (class "thumbnail"))
         (div (@ (class "caption"))
              (div (@ (class "page-header"))
                   (table (@ (class "table"))
                          (h1 ,(post-ref post 'title))
                          (p ,(date->string* (post-date post)))))
              (div (@ (id "post")) ,(post-sxml post))
              (br)
              (script (@ (src "/static/js/disqus.js")))))))

(define (bootstrap-collection-template site title posts prefix)
  (define (post-uri post)
    (string-append (or prefix "") "/"
                   (site-post-slug site post) ".html"))

  `((table (@ (class "table table-striped table-borderless"))
           ,@(map (lambda (post)
                    `(tr (td (a (@ (href ,(post-uri post)))
                                ,(post-ref post 'title)))
                         (td (@ (class "collection-td-date"))
                             ,(date->string* (post-date post)))))
                  posts))))

(define bootstrap-theme
  (theme #:name "bootstrap"
         #:layout bootstrap-layout
         #:post-template bootstrap-post-template
         #:collection-template bootstrap-collection-template))

(site #:title "WiGust Blog"
      #:domain "localhost"
      #:default-metadata '((author . "Oleg Pykhalov")
                           (email  . "go.wigust@gmail.com"))
      #:readers (list commonmark-reader)
      #:builders (list (static-directory "guix-root-static/share/static"
                                         "static")
                       (static-directory "static" "static")
                       (static-directory "deploy" "")
                       (atom-feed #:blog-prefix "feeds"
                                  #:max-entries 10)
                       (blog #:theme bootstrap-theme))
      #:build-directory "site"
      #:file-filter (make-file-filter '()))
