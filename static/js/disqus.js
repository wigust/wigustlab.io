// Origin <https://internet-inspired.com/wrote/load-disqus-on-demand/>.
$(document).ready(function() {
    // Origin <https://stackoverflow.com/a/3265514>.
    $("#post").append("<button class='btn btn-default center-block'\
 id='show-comments'>\ Load Disqus comments</button>\
<div id='disqus_thread'></div>");

    $("#show-comments").on('click', function(){
          $.ajax({
                  type: "GET",
                  url: "https://wigust.disqus.com/embed.js",
                  dataType: "script",
                  cache: true
          });

          // Hide the button once comments load
          $(this).fadeOut();
    });
});
